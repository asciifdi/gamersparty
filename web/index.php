<?php
  include('t_header.php');
  include('meta-creator.php');
  include('t_footer.php');
?>

<!doctype html>
<html lang="es">
  <head>
    <?php escupeMeta(); ?>

  </head>
  <body>
    <div id="main-container" class="container">
      <?php getHeader(); ?>
      <!-- Deploy test -->
      <div class="container">
        <!--<h2 class="text-center mb-4">Volvemos del 16/03 al 20/03</h2>-->
        <h2 class="text-center mb-4">Suspensión Gamersparty 2020</h2>
        <h5 class="text-center my-4" id="timer"></h5>

        <p><b>Organización Gamersparty España</b>, 9 de Marzo de 2020</p>
        <p>Estimados espectadores, socios, voluntarios,<br>
        Debido a la suspensión de la actividad docente por la expansión del coronavirus, se ha decidido posponer el
        evento Gamersparty hasta tener una fecha clara.<br>

        Desde la Organización se piden disculpas a los colaboradores y asistentes del evento.<br>
        Un gran saludo,<br>
        <b>Gamersparty 2020</b></p>
      </div>

      <div class="container d-none">
        <div id="twitch-frame"></div>
      </div> 
        
      <div class="container">
        <!--
        <p class="mt-5 mb-3">Y, ahora, sin más dilación, os dejamos una lista de reproducción de trailers y promos de este año.</p>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLlwfDHLCUECT-ZTB4lyDtqlCwj9GM1l9D" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>-->

      <?php getFooter(); ?>
    </div>
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/code.js"></script>

    <script src="https://embed.twitch.tv/embed/v1.js"></script>
		<script type="text/javascript">

				var embed = new Twitch.Embed("twitch-frame", {
					width: "100%",
					channel: "gamersparty_es",
					layout: "video",
					autoplay: false
				});

				embed.addEventListener(Twitch.Embed.VIDEO_READY, () => {
					var player = embed.getPlayer();
					player.pause();
				});

		//Redimensiona el tamaño del frame para ajustarlo a las pantallas
			$(function(){
				var $window = $(window).on('resize', function(){
					var iframe = $("#stream-frame > iframe");
					$(iframe).height($(iframe).width()*(9/16)+40);
				}).trigger('resize'); //on page load
			});

			$(function(){
				var $window = $(window).on('resize', function(){
					var iframe = $("#twitch-frame > iframe");
					$(iframe).height($(iframe).width()*(9/16)+40);
				}).trigger('resize'); //on page load
			});
      </script>  
  </body>
</html>
