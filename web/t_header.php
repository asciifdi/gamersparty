<?php

function getHeader(){
    $cur = $_SERVER['REQUEST_URI'];
?>

<div id="logo-container" class="container-fluid text-center my-5">
    <h1 id="gpty-title" class="text-center"><span class="flickering-1">G</span>P<span class="flickering-2">T</span>Y</h1>
    <h1 id="gpty-name" class="text-center"><span class="flickering-4">G</span>AM<span class="flickering-1">E</span>R<span class="flickering-3">S</span>P<span class="flickering-2">A</span>RT<span class="flickering-5">Y</span> <span class="pulsating">2<span class="flickering-2">0</span>2<span class="flickering-2">0</span></span></h1>

</div>

<nav class="navbar navbar-expand-md navbar-dark bg-transparent my-3">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div id="test" class="navbar-nav nav nav-fill w-100">
        <?php
            class MenuItem {
                const FORMATSTRING = '<a %s class="nav-item nav-link boton %s" href="%s" target="%s">%s</a>';

                protected $href;
                protected $title;
                protected $class;
                protected $id;
                protected $target;

                function __construct($href, $title, $class="blanco", $id="", $target="") {
                    $this->href = $href;
                    $this->title = $title;
                    $this->class = $class;
                    $this->id = $id;
                    $this->target = $target;
                }
                
                public function html() {
                    return sprintf(MenuItem::FORMATSTRING, 
                        (!empty($this->id))?(sprintf('id="%s"', $this->id)):"", 
                        $this->class.(($_SERVER['REQUEST_URI'] == $this->href)?" active":""),
                        $this->href,
                        $this->target,
                        $this->title);
                }
            }

            class MenuItemDropdown extends MenuItem {
                const FORMATSTRING = MenuItem::FORMATSTRING;
                protected $children;

                function __construct($title, $class="", $children=[], $last=false, $id="") {
                    parent::__construct("#", $title, $class, $id);
                    $this->children = $children;
                    $this->last = ($last) ? "dropdown-menu-md-right" : "";
                }

                public function html() {
                    $string = <<<HTML
<div class="nav-item dropdown">
    <a class="nav-link dropdown-toggle boton $this->class" href="#" id="navbarDropdown-$this->id" 
        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        $this->title
    </a>
    <div class="dropdown-menu $this->last" aria-labelledby="navbarDropdown">
HTML;

                    foreach($this->children as $child) {
                        $string .= <<<HTML
        <a class="dropdown-item py-2 boton $child->class" href="$child->href" target="$child->target">$child->title</a>
HTML;
                    }

                    $string .= <<<HTML
    </div>
HTML;
                    return $string . "</div>";
                }
            }

            $menu = [
                new MenuItem("/", "Inicio", "azul"),
                new MenuItem("/evento", "El Evento", "blanco"),
                // new MenuItem("/speedruns", "Speedruns", "rojo"),
                new MenuItem("/participa", "Participa", "amarillo"),
        		new MenuItem("/gamejam", "Game Jam", "verde"),
                new MenuItemDropdown("Torneos", "rojo", [
                    new MenuItem('https://forms.gle/DSUZbpqiT9yKu9bf6', 'SSBU', "rojo", "", "_blank")
                    # new MenuItem('#', 'Just Dance', "rojo disabled")
                ], true)
                // new MenuItem("/donar", "Donar", "azul", "donar"),
                // new MenuItem("/visita-inta", "Visita INTA", "verde"),
            ];

            foreach($menu as $item) {
                echo $item->html();
            }
        ?>

    </div>
  </div>
</nav>

<?php
}
?>
